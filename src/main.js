import api from './api';

class Api {
    constructor (){
        //Este repositorio irá guardar objetos.
        this.repositories = [];
        //seletor do document
        this.formEl = document.getElementById("repo-form");
        //id da lista com os dados!
        this.listEl = document.getElementById("repo-list");
        //referenciar o input para pegar o valor
        this.input = document.querySelector('input[name=repository]');
        //função de registrar objetos.
        this.registerHandles();
    }

    //registrar os eventos
    //submit etc
    registerHandles(){
        //this.formEl.onsubmit = function (event) {}

        //com es6+ 
        this.formEl.onsubmit = event => this.addRepository(event);
    }

    //fazer o loading adicionando como true o carregamento. 
    setLoading(loading = true){
        if(loading === true){
            //criar span para receber carregando !
            let loadingEl = document.createElement('span');
            //recebe Carregando no documento loadingEl.
            loadingEl.appendChild(document.createTextNode('Carregando...'));
            //setando atributos!
            loadingEl.setAttribute('id', 'loading');

            //adicionando ao formulário!
            this.formEl.appendChild(loadingEl);
        }else{
            //quando não for!
            document.getElementById('loading').remove();
        }
    }

    /*
        Async 
    */

    async addRepository(event){
        //inibi ações do formulario como redenrização da página etc
        event.preventDefault();

        //pegando o valor do input
        const repoInput = this.input.value;

        console.log(repoInput);

        //se o valor não estiver vazio!
        if(repoInput.length === 0)
            return;

        //chamando loading!
        this.setLoading();

        try {
            const response = await api.get(`/repos/${repoInput}`);

            console.log(response);

            /**
             *  Vamos utilizar métodos de destruturação para consumir api
             */

            const { name , description , html_url , owner : { avatar_url } } = response.data;

            this.repositories.push({
                name,
                description,
                avatar_url,
                html_url,
            });

            this.input.value = ``;
            
            this.render();
            console.log(this.repositories);
        }catch(err){
            alert('O repositorio não existe!');
        }

        this.setLoading(false);
    }

    render(){
        //atualizar os campos quando solicitar nova request da API.
        //Iremos apagar a lista para redenrizar com uma nova e atualizada!
        this.listEl.innerHTML = ''; 

        //Percorrer o repositorios

        //vamos utilizar o forEach a funcionalidade percorrer todo o array

        this.repositories.forEach(repo => {
            //criando a imagem
            let imgEl =  document.createElement('img');
            //setando os atributos setAttribute('atributo', valor);
            imgEl.setAttribute('src',repo.avatar_url);

            //Criar elemento do titulo.
            let titleEl = document.createElement('strong');
            titleEl.appendChild(document.createTextNode(repo.name)); //atribuindo o texto ao documento strong

            //Criar a Descrição
            let descriptionEl = document.createElement('p');
            descriptionEl.appendChild(document.createTextNode(repo.description));

            //link
            let linkEl = document.createElement('a');
            linkEl.setAttribute('target','_blank');
            linkEl.setAttribute('href', repo.html_url);
            
            //Criando o texto do Link!
            linkEl.appendChild(document.createTextNode('Acessar!'));


            //Criando nossa li.
            let listItemEl = document.createElement('li');

            listItemEl.appendChild(imgEl);
            listItemEl.appendChild(titleEl);
            listItemEl.appendChild(descriptionEl);
            listItemEl.appendChild(linkEl);
            
            this.listEl.appendChild(listItemEl);
        });
    }

}

new Api;